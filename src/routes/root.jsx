import React from "react";

function Root(props){
    return(
      <>
        <header>
    <div className="container">
      <div className="headerleft">
        <img className="logo" src="" alt="logo" />
        <button className="category">
          <span className="material-symbols-outlined">grid_view</span>
          <span>Category</span>
        </button>
      </div>
      <div className="headercentre">
        <button className="centre">
          Demos<span className="material-symbols-outlined">expand_more</span>
        </button>
        <button className="centre">
          Pages<span className="material-symbols-outlined">expand_more</span>
        </button>
        <button className="centre">
          Accounts<span className="material-symbols-outlined">expand_more</span>
        </button>
        <button className="centre">
          Megamenu<span className="material-symbols-outlined">expand_more</span>
        </button>
      </div>
      <div>
        <form className="searche" action="/search" method="get">
          <input type="text" placeholder="Search..." />
          <input type="submit" defaultValue="Search" />
        </form>
        <span id="menus" className="material-symbols-outlined">
          menu
        </span>
      </div>
    </div>
  </header>
  <footer></footer>
      </>
    )
}

export default Root;